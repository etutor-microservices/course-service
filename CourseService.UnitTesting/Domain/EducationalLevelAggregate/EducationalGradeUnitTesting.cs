using CourseService.Domain.EducationalLevelAggregate;
using FluentAssertions;
using Xunit;

namespace CourseService.UnitTesting.Domain.EducationalLevelAggregate
{
    public class EducationalGradeUnitTesting
    {
        private readonly EducationalGrade _educationalGrade = new("First Grade");

        [Fact]
        public void Creation_ShouldReturnExpectedEducationalGrade()
        {
            var name = "First Grade";

            var educationalGrade = new EducationalGrade(name);

            educationalGrade.Name.Should().Be(name);
        }

        [Fact]
        public void UpdateName_ShouldReturnUpdatedName()
        {
            var updatedName = "1st Grade";

            _educationalGrade.UpdateName(updatedName);

            _educationalGrade.Name.Should().Be(updatedName);
        }

        [InlineData("")]
        [InlineData(null)]
        [Theory]
        public void UpdateName_InvalidInput_ShouldNotUpdateName(string updatedName)
        {
            _educationalGrade.UpdateName(updatedName);

            _educationalGrade.Name.Should().NotBe(updatedName);
        }
    }
}