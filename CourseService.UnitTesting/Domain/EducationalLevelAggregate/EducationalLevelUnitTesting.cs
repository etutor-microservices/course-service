using System.Collections.Generic;
using System.Linq;
using CourseService.Domain.EducationalLevelAggregate;
using FluentAssertions;
using Xunit;

namespace CourseService.UnitTesting.Domain.EducationalLevelAggregate
{
    public class EducationalLevelUnitTesting
    {
        private readonly EducationalLevel _level = new("Pre-School");
        private readonly EducationalGrade _grade = new("First Grade");

        private readonly List<EducationalGrade> _gradeList = new()
        {
            new EducationalGrade("First Grade"),
            new EducationalGrade("Second Grade")
        };

        [Fact]
        public void Creation_ShouldReturnExpectedEducationalLevel()
        {
            var name = "Pre-School";

            var level = new EducationalLevel(name);

            level.Name.Should().Be(name);
            level.Grades.Should().BeEmpty();
        }

        [Fact]
        public void UpdateName_ShouldReturnUpdatedName()
        {
            var updatedName = "Kindergarten";

            _level.UpdateName(updatedName);
        }

        [InlineData("")]
        [InlineData(null)]
        [Theory]
        public void UpdateName_InvalidInput_ShouldNotUpdateName(string updatedName)
        {
            _level.UpdateName(updatedName);

            _level.Name.Should().NotBe(updatedName);
        }

        [Fact]
        public void AddGrade_ShouldAddNewGrade()
        {
            _level.AddGrade(_grade);

            _level.Grades.Count.Should().Be(1);
            _level.Grades.First().Should().Be(_grade);
        }

        [Fact]
        public void AddGrade_DuplicateGrade_ShouldNotAddNewGrade()
        {
            _level.AddGrade(_grade);
            _level.AddGrade(_grade);

            _level.Grades.Count.Should().Be(1);
        }

        [Fact]
        public void AddGrades_ShouldAddGrades()
        {
            _level.AddGrades(_gradeList);

            _level.Grades.Count.Should().Be(_gradeList.Count);
        }

        [Fact]
        public void AddGrades_DuplicateGrades_ShouldNotAddGrades()
        {
            _level.AddGrades(_gradeList);
            _level.AddGrades(_gradeList);

            _level.Grades.Count.Should().Be(_gradeList.Count);
        }

        [Fact]
        public void AddGrades_DuplicateGrade_ShouldNotAddGrades()
        {
            var gradeList = new List<EducationalGrade>()
            {
                new("Second Grade"),
                new("Third Grade")
            };

            _level.AddGrades(_gradeList);
            _level.AddGrades(gradeList);

            _level.Grades.Count.Should().Be(_gradeList.Count + 1);
        }

        [Fact]
        public void DeleteGrade_ShouldDeleteGrade()
        {
            _level.AddGrade(_grade);
        }
    }
}