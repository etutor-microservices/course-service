using System;
using CourseService.Domain.CourseAggregate;
using FluentAssertions;
using Xunit;

namespace CourseService.UnitTesting.Domain.CourseAggregate
{
    public class CourseUnitTesting
    {
        [Fact]
        public void Creation_ShouldReturnExpectedCourse()
        {
            var name = "Math";
            var tutorId = Guid.NewGuid();

            var course = new Course(name, tutorId);

            course.Name.Should().Be(name);
            course.TutorId.Should().Be(tutorId);
        }
    }
}
