using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CourseService.Application.Contracts.Persistence;
using CourseService.Application.Features.EducationalLevels.Commands.CreateNewEducationalLevel;
using CourseService.Application.MappingProfile;
using CourseService.Domain.EducationalLevelAggregate;
using FluentAssertions;
using Moq;
using Xunit;

namespace CourseService.UnitTesting.Application.Features.EducationalLevels
{
    public class CreateNewEducationalLevelUnitTesting
    {
        private readonly Mock<IEducationalLevelsRepository> _mockRepository;
        private readonly CreateNewEducationalLevelValidator _validator;
        private readonly IMapper _mapper;

        private readonly EducationalLevel _educationalLevel = new("First Grade");

        public CreateNewEducationalLevelUnitTesting()
        {
            _mockRepository = new Mock<IEducationalLevelsRepository>();
            _validator = new CreateNewEducationalLevelValidator();
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()).CreateMapper();

            _mockRepository.Setup(x => x.AddAsync(It.IsAny<EducationalLevel>())).ReturnsAsync(_educationalLevel);
        }

        [Fact]
        public async Task ValidParams_ShouldCreateNewAndReturnExpectedLevel()
        {
            var request = new CreateNewEducationalLevel()
            {
                Name = "First Grade"
            };

            var handler = new CreateNewEducationalLevelHandler(_mockRepository.Object, _validator, _mapper);
            var (errors, _) = await handler.Handle(request, default);

            errors.Should().BeNull();
            _mockRepository.Verify(x => x.AddAsync(It.IsAny<EducationalLevel>()));
        }

        [InlineData("")]
        [InlineData(null)]
        [InlineData(
            "FoxIP6bjjpUhwZYWQyihL1LMsGtlBKLw4V9B2xyKor2cW0vSnoHZUkTt0tV61a3a7HUE846kGErMgiNvdFBx8waD6hO3txF8PSvz" +
            "Ly9H7ByMdpahzFbKtCYJPBPPr5oHiOFYAD015kFN0GP6wPm6PfJYEBLG5Z9fE75XwWBuhuAvC3tSf6mZa4lp2zPtVk07Mo4EbV1Bh" +
            "ihwrUXIxmKbQ4hgt6R07WfdsBI7et6YZ1an83lHZcJBlioPlo0")]
        [Theory]
        public async Task InvalidName_ShouldCreateNewAndReturnExpectedLevel(string name)
        {
            var request = new CreateNewEducationalLevel()
            {
                Name = name
            };

            var handler = new CreateNewEducationalLevelHandler(_mockRepository.Object, _validator, _mapper);
            var (errors, _) = await handler.Handle(request, default);

            errors.Should().NotBeNull();
            _mockRepository.Verify(x => x.AddAsync(It.IsAny<EducationalLevel>()), Times.Never);
        }
    }
}