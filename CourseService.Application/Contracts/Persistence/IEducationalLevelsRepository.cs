using System.Threading.Tasks;
using CourseService.Domain.EducationalLevelAggregate;

namespace CourseService.Application.Contracts.Persistence
{
    public interface IEducationalLevelsRepository : IAsyncRepository<EducationalLevel>,
        IAsyncReadRepository<EducationalLevel>
    {
        Task<EducationalLevel> GetByNameAsync(string name);
    }
}