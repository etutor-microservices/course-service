using System;
using System.Threading.Tasks;
using CourseService.Domain.Common;

namespace CourseService.Application.Contracts.Persistence
{
    public interface IAsyncReadRepository<T> where T : class, IAggregateRoot
    {
        Task<T> GetByIdAsync(Guid id);
    }
}