using System.Threading.Tasks;
using CourseService.Application.Features.EducationalLevels.Queries.GetEducationalLevelByName;
using FluentValidation;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Commands.CreateNewEducationalLevel
{
    public class CreateNewEducationalLevelValidator : AbstractValidator<CreateNewEducationalLevel>
    {
        public CreateNewEducationalLevelValidator()
        {
            RuleFor(cne => cne.Name).MaximumLength(250).NotNull().NotEmpty();
        }
    }
}