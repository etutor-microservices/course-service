using System.Collections.Generic;
using CourseService.Application.Features.EducationalLevels.ViewModels;
using CourseService.Domain.EducationalLevelAggregate;
using FluentValidation.Results;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Commands.CreateNewEducationalLevel
{
    public class
        CreateNewEducationalLevel : IRequest<(List<ValidationFailure> errors, EducationalLevelVm educationalLevel)>
    {
        public string Name { get; init; }
    }
}