using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CourseService.Application.Contracts.Persistence;
using CourseService.Application.Features.EducationalLevels.ViewModels;
using CourseService.Domain.EducationalLevelAggregate;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Commands.CreateNewEducationalLevel
{
    public class CreateNewEducationalLevelHandler : IRequestHandler<
        CreateNewEducationalLevel, (List<ValidationFailure> errors, EducationalLevelVm educationalLevel)>
    {
        private readonly IEducationalLevelsRepository _educationalLevelsRepository;
        private readonly IValidator<CreateNewEducationalLevel> _validator;
        private readonly IMapper _mapper;

        public CreateNewEducationalLevelHandler(IEducationalLevelsRepository educationalLevelsRepository,
            IValidator<CreateNewEducationalLevel> validator, IMapper mapper)
        {
            _educationalLevelsRepository = educationalLevelsRepository ??
                                           throw new ArgumentNullException(nameof(educationalLevelsRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(List<ValidationFailure> errors, EducationalLevelVm educationalLevel)> Handle(
            CreateNewEducationalLevel request, CancellationToken cancellationToken)
        {
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            var educationalLevel = new EducationalLevel(request.Name);

            var insertedEducationalLevel = await _educationalLevelsRepository.AddAsync(educationalLevel);

            return (null, _mapper.Map<EducationalLevelVm>(insertedEducationalLevel));
        }
    }
}