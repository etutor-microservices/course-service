using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CourseService.Application.Features.EducationalLevels.ViewModels;
using FluentValidation.Results;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Commands.UpdateEducationalLevel
{
    public class UpdateEducationalLevelHandler : IRequestHandler<UpdateEducationalLevel, (List<ValidationFailure> errors
        , EducationalLevelVm educationalLevel)>
    {
        public UpdateEducationalLevelHandler()
        {
        }

        public async Task<(List<ValidationFailure> errors, EducationalLevelVm educationalLevel)> Handle(
            UpdateEducationalLevel request, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}