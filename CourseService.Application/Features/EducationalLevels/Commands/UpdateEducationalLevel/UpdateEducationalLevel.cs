using System.Collections.Generic;
using CourseService.Application.Features.EducationalLevels.ViewModels;
using CourseService.Domain.EducationalLevelAggregate;
using FluentValidation.Results;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Commands.UpdateEducationalLevel
{
    public class
        UpdateEducationalLevel : IRequest<(List<ValidationFailure> errors, EducationalLevelVm educationalLevel)>
    {
        public EducationalLevel EducationalLevel { get; set; }
        public string Name { get; init; }
    }
}