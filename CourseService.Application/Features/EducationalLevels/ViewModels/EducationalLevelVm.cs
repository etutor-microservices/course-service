using System;

namespace CourseService.Application.Features.EducationalLevels.ViewModels
{
    public class EducationalLevelVm
    {
        public Guid Id { get; init; }
        public string Name { get; init; }
    }
}