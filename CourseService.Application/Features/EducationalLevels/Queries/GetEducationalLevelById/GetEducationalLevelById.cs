using System;
using CourseService.Domain.EducationalLevelAggregate;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Queries.GetEducationalLevelById
{
    public class GetEducationalLevelById : IRequest<EducationalLevel>
    {
        public Guid Id { get; init; }
    }
}