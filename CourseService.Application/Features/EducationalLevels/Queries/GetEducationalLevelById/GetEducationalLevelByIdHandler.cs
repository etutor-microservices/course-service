using System;
using System.Threading;
using System.Threading.Tasks;
using CourseService.Application.Contracts.Persistence;
using CourseService.Domain.EducationalLevelAggregate;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Queries.GetEducationalLevelById
{
    public class GetEducationalLevelByIdHandler : IRequestHandler<GetEducationalLevelById, EducationalLevel>
    {
        private readonly IEducationalLevelsRepository _educationalLevelsRepository;

        public GetEducationalLevelByIdHandler(IEducationalLevelsRepository educationalLevelsRepository)
        {
            _educationalLevelsRepository = educationalLevelsRepository ??
                                           throw new ArgumentNullException(nameof(educationalLevelsRepository));
        }

        public async Task<EducationalLevel> Handle(GetEducationalLevelById request, CancellationToken cancellationToken)
        {
            return await _educationalLevelsRepository.GetByIdAsync(request.Id);
        }
    }
}