using CourseService.Domain.EducationalLevelAggregate;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Queries.GetEducationalLevelByName
{
    public class GetEducationalLevelByName : IRequest<EducationalLevel>
    {
        public string Name { get; init; }
    }
}