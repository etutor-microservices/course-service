using System;
using System.Threading;
using System.Threading.Tasks;
using CourseService.Application.Contracts.Persistence;
using CourseService.Domain.EducationalLevelAggregate;
using MediatR;

namespace CourseService.Application.Features.EducationalLevels.Queries.GetEducationalLevelByName
{
    public class GetEducationalLevelByNameHandler : IRequestHandler<GetEducationalLevelByName, EducationalLevel>
    {
        private readonly IEducationalLevelsRepository _educationalLevelsRepository;

        public GetEducationalLevelByNameHandler(IEducationalLevelsRepository educationalLevelsRepository)
        {
            _educationalLevelsRepository = educationalLevelsRepository ??
                                           throw new ArgumentNullException(nameof(educationalLevelsRepository));
        }

        public async Task<EducationalLevel> Handle(GetEducationalLevelByName request,
            CancellationToken cancellationToken)
        {
            return await _educationalLevelsRepository.GetByNameAsync(request.Name);
        }
    }
}