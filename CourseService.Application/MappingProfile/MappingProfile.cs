using AutoMapper;
using CourseService.Application.Features.EducationalLevels.ViewModels;
using CourseService.Domain.EducationalLevelAggregate;

namespace CourseService.Application.MappingProfile
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EducationalLevel, EducationalLevelVm>();
        }
    }
}