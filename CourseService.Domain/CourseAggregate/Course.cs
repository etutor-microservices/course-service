using System;

namespace CourseService.Domain.CourseAggregate
{
    public class Course
    {
        public string Name { get; private set; }
        public Guid TutorId { get; private set; }

        public Course(string name, Guid tutorId)
        {
            Name = name;
            TutorId = tutorId;
        }
    }
}