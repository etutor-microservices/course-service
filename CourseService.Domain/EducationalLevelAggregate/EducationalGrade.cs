using CourseService.Domain.Common;

namespace CourseService.Domain.EducationalLevelAggregate
{
    public class EducationalGrade : BaseEntity<int>
    {
        public string Name { get; private set; }

        private EducationalGrade()
        {
        }

        public EducationalGrade(string name)
        {
            Name = name;
        }

        public void UpdateName(string name)
        {
            if (string.IsNullOrEmpty(name)) return;
            Name = name;
        }
    }
}