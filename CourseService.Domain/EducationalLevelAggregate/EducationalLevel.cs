using System;
using System.Collections.Generic;
using System.Linq;
using CourseService.Domain.Common;

namespace CourseService.Domain.EducationalLevelAggregate
{
    public class EducationalLevel : BaseEntity<Guid>, IAggregateRoot
    {
        public string Name { get; private set; }

        private readonly List<EducationalGrade> _grades = new();
        public IReadOnlyCollection<EducationalGrade> Grades => _grades.AsReadOnly();

        private EducationalLevel()
        {
        }

        public EducationalLevel(string name)
        {
            Name = name;
        }

        public void UpdateName(string name)
        {
            if (string.IsNullOrEmpty(name)) return;
            Name = name;
        }

        public void AddGrade(EducationalGrade grade)
        {
            if (_grades.Any(g => g.Name == grade.Name)) return;
            _grades.Add(grade);
        }

        public void AddGrades(IEnumerable<EducationalGrade> gradeList)
        {
            foreach (var grade in gradeList) AddGrade(grade);
        }
    }
}