using System.Linq;
using System.Threading.Tasks;
using CourseService.Application.Contracts.Persistence;
using CourseService.Domain.EducationalLevelAggregate;
using Microsoft.EntityFrameworkCore;

namespace CourseService.Persistence.Repositories
{
    public class EducationalLevelsRepository : BaseRepository<EducationalLevel>, IEducationalLevelsRepository
    {
        public EducationalLevelsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<EducationalLevel> GetByNameAsync(string name)
        {
            return await DbContext.Set<EducationalLevel>().Where(e => e.Name == name).FirstOrDefaultAsync();
        }
    }
}