using System;
using System.Threading.Tasks;
using CourseService.Application.Contracts.Persistence;
using CourseService.Domain.Common;

namespace CourseService.Persistence.Repositories
{
    public class BaseRepository<T> : IAsyncRepository<T>, IAsyncReadRepository<T> where T : class, IAggregateRoot
    {
        protected ApplicationDbContext DbContext { get; }

        public BaseRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await DbContext.Set<T>().FindAsync(id);
        }

        public async Task<T> AddAsync(T entity)
        {
            var inserted = DbContext.Add(entity).Entity;
            await DbContext.SaveChangesAsync();
            return inserted;
        }
    }
}