using CourseService.Domain.EducationalLevelAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CourseService.Persistence.Configurations
{
    public class EducationalGradeConfiguration : IEntityTypeConfiguration<EducationalGrade>
    {
        public void Configure(EntityTypeBuilder<EducationalGrade> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).HasMaxLength(250);

            builder.HasIndex(e => e.Name).IsUnique();
        }
    }
}