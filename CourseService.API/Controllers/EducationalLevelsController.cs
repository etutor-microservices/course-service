using System;
using System.Threading.Tasks;
using CourseService.Application.Features.EducationalLevels.Commands.CreateNewEducationalLevel;
using CourseService.Application.Features.EducationalLevels.Commands.UpdateEducationalLevel;
using CourseService.Application.Features.EducationalLevels.Queries.GetEducationalLevelById;
using CourseService.Application.Features.EducationalLevels.Queries.GetEducationalLevelByName;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CourseService.API.Controllers
{
    [ApiController]
    [Route("educational-levels")]
    public class EducationalLevelsController : BaseController
    {
        private readonly IMediator _mediator;

        public EducationalLevelsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet("{id:guid}", Name = "GetEducationalLevelById")]
        public async Task<IActionResult> GetById([FromRoute] GetEducationalLevelById getEducationalLevelById)
        {
            var educationalLevel = await _mediator.Send(getEducationalLevelById);
            if (educationalLevel == null)
                return NotFound($"Educational level with id {getEducationalLevelById.Id} cannot be found!");

            return Ok(educationalLevel);
        }


        [HttpPost]
        public async Task<IActionResult> CreateNew([FromBody] CreateNewEducationalLevel createNewEducationalLevel)
        {
            var educationalLevel = await _mediator.Send(new GetEducationalLevelByName()
                {Name = createNewEducationalLevel.Name});
            if (educationalLevel != null)
                return BadRequest($"Educational level {createNewEducationalLevel.Name} has already exist!");

            var (errors, createdEducationalLevel) = await _mediator.Send(createNewEducationalLevel);
            if (errors != null) return BadRequest(errors);
            return CreatedAtRoute("GetEducationalLevelById", new {id = createdEducationalLevel.Id},
                createdEducationalLevel);
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> Update([FromRoute] GetEducationalLevelById getEducationalLevelById,
            [FromBody] UpdateEducationalLevel updateEducationalLevel)
        {
            var educationalLevel = await _mediator.Send(getEducationalLevelById);
            if (educationalLevel == null)
                return NotFound($"Educational level with id {getEducationalLevelById.Id} cannot be found!");

            updateEducationalLevel.EducationalLevel = educationalLevel;
            var (errors, updatedEducationalLevel) = await _mediator.Send(updateEducationalLevel);
            if (errors != null) return BadRequest(errors);

            return Ok(updatedEducationalLevel);
        }
    }
}